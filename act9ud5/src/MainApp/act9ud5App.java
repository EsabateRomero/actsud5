/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat
 *
 */
public class act9ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);

		int numeroVentas = obtenerNumeroVentas(teclat);
		System.out.println("Nmero de ventas: " + numeroVentas);
		
		
		int preuTotal = 0;
		
		
		int contadorVentas = 0;
		while(contadorVentas<numeroVentas) {
			System.out.println("Introdueix venta: ");
			int venta = teclat.nextInt();
			preuTotal = preuTotal + venta;
			contadorVentas++;
		}
		
		
		System.out.println("Les vostres ventes sumen: " + preuTotal +" euros. " );
			
		
	}
	
	public static int obtenerNumeroVentas(Scanner teclat) {
		
		int numeroVentas = 0;
		boolean numeroVentesValid = false;
		while(!numeroVentesValid) {
			System.out.println("Introdueix el nmero de ventes que vol introduir");
			numeroVentas = teclat.nextInt();
			if(numeroVentas>0)
				numeroVentesValid = true;
			else
				System.out.println("El nmero introdut hauria de ser major que 0");
				
		}
					
		return numeroVentas;
	}
	
}
