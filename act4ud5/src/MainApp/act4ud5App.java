/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act4ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner teclat = new Scanner(System.in);
		boolean salir = false;
		
		while(!salir) {
			System.out.println("CALCULAR EL �REA DE UN C�RCULO (PI*R^2)");
			int opcion = elegirOpcion(teclat);
			System.out.println("la opci� es : " + opcion);
			switch (opcion) {
			case 1:
				areaCirculoScanner(teclat);
				break;
			case 2:
				areaCirculoJOption();
				break;
			case 3:
				salir = true;
				break;
			}
		}
			System.out.println("Heu sortit");
	}
	
	
	public static void areaCirculoScanner(Scanner teclat) {
		System.out.println("A�ada el radio del c�rculo: ");
		double radio = teclat.nextDouble();
		double areaCirculo = Math.PI*(radio*radio);
		System.out.println("El area del ciclo es "+areaCirculo);
	}
	
	
	
	public static void areaCirculoJOption() {
		String radioString = JOptionPane.showInputDialog("A�ada el radio del c�rculo: ");
		double radio = Double.parseDouble(radioString); 
		double areaCirculo = Math.PI*(radio*radio);
		JOptionPane.showMessageDialog(null, "El area del ciclo es "+areaCirculo);		
	}
	
	
	
	public static int elegirOpcion(Scanner teclat) {
		boolean num_valid = false;
		int opcion = 0;
		while(!num_valid) {
		System.out.println("1. Calcular mediante la clase scanner");
		System.out.println("2. Calcular mediante JOptionPane");
		System.out.println("3. Salir");
		System.out.println("-----------------------------");
		System.out.println("Opci�n: ");

		opcion = teclat.nextInt();
		if(opcion==1||opcion==2||opcion==3)	
			num_valid = true;	
		else 
			System.out.println("Ha de triar la opci� 1, 2 o 3. La opci� "+opcion+" no es troba disponible.");
		
		}
		return opcion;	
	}
	
}
