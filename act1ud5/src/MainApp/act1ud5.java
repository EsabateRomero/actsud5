/**
 * 
 */
package MainApp;

/**
 * @author Elisabet Sabat�
 *
 */
public class act1ud5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int num1 = 5, num2 = 10;
		
		if(num1>num2) 
			System.out.println("El primer n�mero introduit �s m�s gran, �s a dir, el n�mero m�s gran �s "+num1);
		else if (num1<num2)
			System.out.println("El segon n�mero introduit �s m�s gran, �s a dir, el n�mero m�s gran �s "+num2);
		else
			System.out.println("Els dos n�meros s�n iguals");
	}

}
