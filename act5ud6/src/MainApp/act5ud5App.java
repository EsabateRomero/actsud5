/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act5ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner (System.in);
		System.out.println("N�mero a comprobar:");
		int numero = teclat.nextInt();
		if(numero%2==0)
			System.out.println("El n�mero �s multiple de 2");
		else
			System.out.println("El n�mero no �s multiple de 2");
		
	}
	
}
