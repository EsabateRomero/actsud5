/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act13ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Hola, bones, s�c una calculadora,"
				+ "quina operaci� vols que et calculi?");
		System.out.println("numeros: ");
		int num1= teclat.nextInt();
		int num2= teclat.nextInt();
		System.out.println("signe");
		String signe = teclat.next();
		boolean signeNoTrobat = false;
		String operacio = num1+signe+num2;
		double resultat = 0;
		switch (signe) {
		case "/":
			resultat = num1/num2;
			break;

		case "%":
			resultat = num1%num2;
			break;

			
		case "+":
			resultat = num1+num2;
			break;
	
		case "-":
			resultat = num1-num2;
			break;
	
		case "*":
			resultat = num1*num2;
			break;

		default:
			signeNoTrobat = true;
		
		}
		
		if(!signeNoTrobat)
			System.out.println(operacio+ " = "+resultat);
		else
			System.out.println("No s'ha trobat el signe solicitat");
		
	}
	
}
