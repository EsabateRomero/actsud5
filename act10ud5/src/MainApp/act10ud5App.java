/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act10ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
				
		System.out.println("Diga un d�a de la semana:");
		String dia = teclat.next(); 
		boolean festivo;
		switch (dia) {
		case "dilluns":
			festivo = false;
			opcions(festivo);
			break;
		case "dimarts":
			festivo = false;
			opcions(festivo);
			break;
			
		case "dimecres":
			festivo = false;
			opcions(festivo);
			break;
			
		case "dijous":
			festivo = false;
			opcions(festivo);
			break;

		case "divendres":
			festivo = false;
			opcions(festivo);
			break;

		case "dissabte":
			festivo = true;
			opcions(festivo);
			break;

		case "diumenge":
			festivo = true;
			opcions(festivo);
			break;
			
		default:
			System.out.println("No es troba la opci�");
			break;
		}
		
		
	}
	
	public static void opcions(boolean festivo) {
		if(!festivo)
			System.out.println("El dia seleccionat no es festivo");
			else
			System.out.println("El dia seleccionat es festivo");
	}
	
}
