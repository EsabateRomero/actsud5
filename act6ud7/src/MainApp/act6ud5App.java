/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act6ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		double IVA = 0.21;
		Scanner teclat = new Scanner(System.in);
		System.out.println("Introdueix el preu del producte per a que poguem calcular l'IVA");
		double precioProducto = teclat.nextDouble();
		double precioProductoIVA = precioProducto + precioProducto * IVA;
		System.out.println("El preu es "+precioProductoIVA);
		
	}
	
}
