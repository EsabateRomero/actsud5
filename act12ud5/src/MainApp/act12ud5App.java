/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act12ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		
		String contra = "hola";
		int numeroIntentos = 0;
		
		boolean contraEncertada = false;
		
		while(!contraEncertada) {
			
			System.out.println("Introdueix la contrassenya:");
			String contrassenyaUsuari = teclat.next();
			
			if(contrassenyaUsuari!=contra)
				numeroIntentos++;
			
			if(contrassenyaUsuari.contentEquals(contra))
				contraEncertada = true;
				
			if(numeroIntentos==3) {
				contraEncertada = false;
				break;
			}
				
		}
		
		if(contraEncertada)
			System.out.println("Heu encertat la contrassenya");
		else
			System.out.println("Heu fallat la contrassenya amb tres intents");
			
		
	}
	
}
